package espn.cricinfo;

import java.util.ArrayList;
import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CricinfoLivescorePage extends Initializer {
	protected List<String> url = new ArrayList<String>();
	protected ArrayList<WebElement> matches = (ArrayList<WebElement>) driver
			.findElements(By.xpath("//div[@class='match-score-block']/div[2]/a"));
	WebDriverWait wait = new WebDriverWait(driver, 5);
	
	public void getMatchLinks() {
		// System.out.println(matches.size());
		for (int i = 1; i <= matches.size(); i++) {
			String match = ((matches.get(i - 1).getAttribute("href")));
			url.add(match);
		}
	}
	public void clickMatchLinks() {
		for (int i = 0; i < 3; i++) {
			driver.navigate().to(url.get(i));
			 wait.until(ExpectedConditions
						.visibilityOfElementLocated(By.xpath("//div[@class='match-header-container']")));
			 CricinfoMatchDetailsPage cmdp = new CricinfoMatchDetailsPage();
				cmdp.getMatchDetails();
				cmdp.toJson();
		}
	}

}
